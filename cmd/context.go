/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/kmpm/yamc/internal"
	"github.com/kmpm/yamc/internal/log"
	"gopkg.in/yaml.v3"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

// contextCmd represents the context command
var contextCmd = &cobra.Command{
	Use:   "context",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("context called")
	},
}

var contextListCmd = &cobra.Command{
	Use:   "list",
	Short: "list configured contexts",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("list contexts")
		list, err := internal.ListContexts()
		if err != nil {
			log.Default.Fatal("error listing contexts", zap.Error(err))
		}
		for _, e := range list {
			fmt.Printf("%s\t%s\t%s\n", e.Context.Name, e.Context.Description, e.Filename)
		}
	},
}
var contextEditCmd = &cobra.Command{
	Use:   "edit",
	Short: "edit context",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Default.Fatal("no context given")
		}
		editor := os.Getenv("EDITOR")

		if editor == "" {
			log.Default.Fatal("no editor configured")
		}

		wantname := args[0]
		list, err := internal.ListContexts()
		if err != nil {
			log.Default.Fatal("error listing contexts", zap.Error(err))
		}
		for _, e := range list {
			if e.Context.Name == wantname {
				bytes, err := yaml.Marshal(e.Context)
				if err != nil {
					log.Default.Fatal("could not marshal context", zap.Error(err))
				}
				bytes, err = edit(bytes, "yml")
				if err != nil {
					log.Default.Fatal("error editing", zap.Error(err))
				}
				ctx := internal.Context{}
				err = yaml.Unmarshal(bytes, &ctx)
				if err != nil {
					log.Default.Fatal("error unmarshalling", zap.Error(err))
				}
				err = internal.SaveContext(&ctx)
				if err != nil {
					log.Default.Fatal("error saving context", zap.Error(err))
				}
			}
		}
	},
}

func edit(data []byte, ext string) ([]byte, error) {
	editor := os.Getenv("EDITOR")
	fil, err := os.CreateTemp("", fmt.Sprintf("*.%s", ext))
	if err != nil {
		return data, err
	}

	filename := fil.Name()
	fil.Write(data)
	fil.Close()

	defer os.Remove(filename)

	cmd := exec.Command(editor, fil.Name())
	if err := cmd.Run(); err != nil {
		log.Default.Fatal("error editing", zap.Error(err))
	}
	bytes, err := os.ReadFile(filename)
	if err != nil {
		return data, err
	}
	return bytes, nil
}

func init() {
	contextCmd.AddCommand(contextListCmd)
	contextCmd.AddCommand(contextEditCmd)
	rootCmd.AddCommand(contextCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// contextCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// contextCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
