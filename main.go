/*
Copyright © 2022 Peter Magnusson <me@kmpm.se>
*/
package main

import "github.com/kmpm/yamc/cmd"

func main() {
	cmd.Execute()
}
