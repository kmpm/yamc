package internal

import (
	"path/filepath"
	"regexp"
	"strings"
	"testing"
)

func Test_getConfigHome(t *testing.T) {
	type args struct {
		extra []string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"first", args{}, strings.ReplaceAll(filepath.Join(".local", "share", "yamc"), "\\", "\\\\"), false},
		{"no parent dir", args{[]string{"..", ".."}}, "", true},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getConfigHome(tt.args.extra...)
			if (err != nil) != tt.wantErr {
				t.Errorf("getConfigHome() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			rewant := regexp.MustCompile(tt.want)
			if !rewant.MatchString(got) {
				t.Errorf("getConfigHome() = %v, want %v", got, tt.want)
			}
		})
	}
}
