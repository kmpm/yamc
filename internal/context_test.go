package internal

import (
	"reflect"
	"testing"
)

func TestLoadContext(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    *Context
		wantErr bool
	}{
		{"default", args{"default"}, &Context{Host: "localhost", Port: 1883}, false},
		{"missing", args{"something"}, nil, true},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LoadContext(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("LoadContext() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LoadContext() = %v, want %v", got, tt.want)
			}
		})
	}
}
