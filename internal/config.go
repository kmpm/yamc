package internal

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func getConfigHome(extra ...string) (string, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}
	basepath := filepath.Join(home, ".local", "share", "yamc")
	if len(extra) > 0 {
		newbase := filepath.Join(append([]string{basepath}, extra...)...)
		newbase = path.Clean(newbase)
		if !strings.HasPrefix(newbase, basepath) {
			return "", fmt.Errorf("invalid configuration folder: %s", newbase)
		}
		basepath = newbase
	}

	if _, err := os.Stat(basepath); err != nil {
		if err := os.MkdirAll(basepath, os.ModePerm); err != nil {
			return "", nil
		}
	}
	return basepath, nil
}
