package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

type Context struct {
	Name        string `yaml:"name"`
	Description string `yaml:"description"`
	Host        string `yaml:"host"`
	Port        uint16 `yaml:"port"`
}

func defaultContext() Context {
	return Context{
		Name:        "default",
		Description: "Default context",
		Host:        "localhost",
		Port:        1883,
	}
}

func contextToFilename(name string) (string, error) {
	//TODO: validate format of name

	basepath, err := getConfigHome("contexts")
	if err != nil {
		return "", err
	}

	filename := filepath.Join(basepath, fmt.Sprintf("%s.yml", name))
	return filename, nil
}

func saveContextFile(filename string, ctx *Context) error {
	data, err := yaml.Marshal(ctx)
	if err != nil {
		return err
	}
	return os.WriteFile(filename, data, os.ModePerm)
}

func loadContextFile(filename string) (*Context, error) {
	var err error
	var data []byte

	data, err = os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var ctx Context
	err = yaml.Unmarshal(data, &ctx)
	if err != nil {
		return nil, err
	}
	return &ctx, nil
}

func LoadContext(name string) (*Context, error) {
	filename, err := contextToFilename(name)
	if err != nil {
		return nil, err
	}
	if _, err := os.Stat(filename); err != nil {
		if name == "default" {
			ctx := defaultContext()
			return &ctx, nil
		}
		return nil, errors.Wrapf(err, "context '%s' doesn't exist", name)
	}
	return loadContextFile(filename)
}

func SaveContext(ctx *Context) error {
	name := ctx.Name
	filename, err := contextToFilename(name)
	if err != nil {
		return err
	}
	return saveContextFile(filename, ctx)
}

type ContextEntry struct {
	Filename string
	Selected bool
	Context  *Context
}

func ListContexts() ([]ContextEntry, error) {
	home, err := getConfigHome("contexts")
	if err != nil {
		return nil, err
	}
	entries, err := os.ReadDir(home)
	out := make([]ContextEntry, 0)
	foundDefault := false
	for _, e := range entries {

		if !e.IsDir() {
			f := e.Name()
			ext := filepath.Ext(f)
			s := strings.TrimSuffix(f, ext)
			if s == "default" {
				foundDefault = true
			}
			cte := ContextEntry{Filename: filepath.Join(home, f)}
			ctx, err := loadContextFile(cte.Filename)
			if err != nil {
				return nil, err
			}
			cte.Context = ctx
			out = append(out, cte)
		} else {
			fmt.Println(e.Name())
		}
	}
	if !foundDefault {
		dctx := defaultContext()
		out = append(out, ContextEntry{Context: &dctx})
	}
	return out, err
}
