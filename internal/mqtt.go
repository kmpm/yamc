package internal

import (
	"sync"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/kmpm/yamc/internal/log"
)

type Manager struct {
	Client     MQTT.Client
	Opts       *MQTT.ClientOptions
	RetryCount int
	Subscribed map[string]byte

	lock *sync.Mutex // use for reconnect
}

func NewManager() *Manager {
	return &Manager{}
}

// Connects connect to the MQTT broker with Options.
func (m *Manager) Connect() (MQTT.Client, error) {
	m.Client = MQTT.NewClient(m.Opts)
	log.Default.Info("connecting...")

	if token := m.Client.Connect(); token.Wait() && token.Error() != nil {
		return nil, token.Error()
	}
	return m.Client, nil
}

func (m *Manager) Disconnect() error {
	if m.Client.IsConnected() {
		m.Client.Disconnect(20)
		log.Default.Info("client disconnected")
	}
	return nil
}
